# spike-aws-sdk-browser

AWS SDK in the browser using Amazon Cognito identity pool to allow unauthenticated user calls to be made to AWS Firehose.

To allow access to your AWS services:

1.  Create an Amazon Cognito identity pool, configured to allow unauthenticated users
    *  Note down the created role name for unauthenticated users
	*  Note down the created Amazon Cognito IdentityPoolId
2.  Attach appropriate policies to the unauthenticated users role in the IAM management console
3.  Specify the Amazon Cognito IdentityPoolId in the following line in the header script
    *  `AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: "IDENDITY_POOL_ID_HERE"});`
